const { model, Schema } = require('mongoose');

const UserSchema = new Schema({
    email: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    userName: {
        type: String,
        unique: true,
        required: true,
    }
})

module.exports = model('User', UserSchema, 'users-postter');