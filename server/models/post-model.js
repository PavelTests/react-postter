const { model, Schema } = require('mongoose');

const PostSchema = new Schema({
    text: {
        type: String,
        required: true,
    },
    authorId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    authorName: {
        type: String,
        ref: 'User',
    },
    title: {
        type: String,
        required: true,
    },
    likedById: {
        type: [Schema.Types.ObjectId],
        ref: 'User',
        default: [],
    },
    likedByName: {
        type: [String],
        default: [],
    }
}, { timestamps: true })

module.exports = model('Post', PostSchema, 'posts-postter');