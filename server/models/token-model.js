const { model, Schema } = require('mongoose');
const TokenSchema = new Schema({
    refreshToken: {
        type: String,
        required: true
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    }
})

module.exports = model('Token', TokenSchema, 'tokens-postter');