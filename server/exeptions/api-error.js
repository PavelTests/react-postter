module.exports = class ApiError extends Error {
    constructor(status, message = '', errors = []) {
        super(message);
        this.status = status;
        this.errors = errors;
    }

    static BadRequest(description = '', errors = []) {
        const message = 'BadRequest: ' + description;
        return new ApiError(400, message, errors);
    }

    static Unauthorized(description = '', errors = []) {
        const message = 'UnauthorizedError: ' + description;
        return new ApiError(401, message, errors);
    }
}