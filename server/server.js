require('dotenv').config();
const express = require('express');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const morgan = require('morgan');

const routerUser = require('./router/router-user');
const routerPost = require('./router/router-post');
const errorMiddleware = require('./middleware/error-middleware');

const app = express();

//for decode cookie in request
app.use(cookieParser());

//for decode json BODY in POST/PUT req
app.use(express.json());

//for set CORS headers in response
//if empty, A-C-Allow-Origin: *
app.use(cors({
    origin: process.env.CLIENT_URL || 'http://localhost:3000',
    credentials: true,
}));

//for logger in console
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));

const DB_URL = 'mongodb+srv://lad-user:Bvs3VqZzs7kfsz7b@cluster0.bhrdpu8.mongodb.net/?retryWrites=true&w=majority';

const start = async () => {
    await mongoose
        .connect(process.env.DB_URL || DB_URL,
            { useNewUrlParser: true, useUnifiedTopology: true })
        .then(() => console.log('Connected MongoBD'))
        .catch(console.log);

    app.listen(process.env.PORT || 5000, () => {
        console.log(`Server started on ${process.env.PORT || 5000}`);
    })
}

start();

app.use('/api', routerUser);
app.use('/api', routerPost);

// app.get('/', (req, res, next) => {
//     console.log('api');
//     next();
// })

app.use(errorMiddleware);

app.use((req, res) => {
    console.log(404, 'Not found');
    res.status(404).json({ message: 'Not found' });
})


