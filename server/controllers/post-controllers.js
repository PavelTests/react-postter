const { validationResult } = require('express-validator');
const ApiError = require('../exeptions/api-error');
const PostService = require('../service/post-service');

module.exports = class PostControllers {
    static async getPosts(req, res, next) {
        try {
            const users = await PostService.getPosts();
            return res.status(200).json(users);
        } catch (error) {
            next(error);
        }
    }

    static async addPost(req, res, next) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                throw ApiError.BadRequest('Invalid post data', errors.array());
            }
            const { text, authorId, title } = req.body;

            const postDoc = await PostService.addPost(text, authorId, title);
            return res.status(200).json(postDoc);
        } catch (error) {
            next(error);
        }
    }

    static async updatePost(req, res, next) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                throw ApiError.BadRequest('Invalid post data', errors.array());
            }
            const id = req.params.id;
            const { text, authorId, title, likedById } = req.body;

            const postDoc = await PostService
                .updatePost(id, { text, authorId, title, likedById });
            return res.status(200).json(postDoc);
        } catch (error) {
            next(error);
        }
    }
}