const { validationResult } = require('express-validator');
const ApiError = require('../exeptions/api-error')
const UserService = require('../service/user-service');

const MAX_AGE_REF = 1000 * 60 * 60 * 24;

module.exports = class UserControllers {
    static async registration(req, res, next) {
        try {
            //Finds the validation errors in this request 
            //and wraps them in an object with handy functions
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                throw ApiError.BadRequest('Invalid registration data', errors.array())
            }

            const { email, password, userName } = req.body;
            const userData = await UserService.registration(email, password, userName);
            res.cookie('refreshToken',
                userData.refreshToken,
                { maxAge: MAX_AGE_REF, httpOnly: true }
            );

            return res.status(200).json(userData);
        } catch (error) {
            next(error);
        }
    }

    static async login(req, res, next) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                throw ApiError.BadRequest('Invalid login data', errors.array());
            }

            const { email, password } = req.body;
            const userData = await UserService.login(email, password);
            res.cookie('refreshToken',
                userData.refreshToken,
                { maxAge: MAX_AGE_REF, httpOnly: true }
            );

            return res.status(200).json(userData);
        } catch (error) {
            next(error);
        }
    }

    static async logout(req, res, next) {
        try {
            const refreshToken = req.cookies.refreshToken;
            if (!refreshToken) {
                throw ApiError.Unauthorized('Missing refreshToken');
            }

            const tokenData = await UserService.logout(refreshToken);
            res.clearCookie('refreshToken',
                { httpOnly: true }
            );
            return res.status(200).json({ tokenData });
        } catch (error) {
            next(error);
        }
    }

    static async refresh(req, res, next) {
        try {
            const refreshToken = req.cookies.refreshToken;
            if (!refreshToken) {
                throw ApiError.Unauthorized('Missing refreshToken');
            }

            const userData = await UserService.refresh(refreshToken);
            res.cookie('refreshToken',
                userData.refreshToken,
                { maxAge: MAX_AGE_REF, httpOnly: true }
            );

            return res.status(200).json(userData);
        } catch (error) {
            next(error);
        }
    }

    static async getUsers(req, res, next) {
        try {
            const users = await UserService.getUsers();
            return res.status(200).json(users);
        } catch (error) {
            next(error);
        }
    }

}
