const PostModel = require('../models/post-model');
const UserModel = require('../models/user-model')

module.exports = class PostService {
    static async getPosts() {
        const posts = await PostModel.find();
        return posts;
    }

    static async addPost(text, authorId, title) {
        const { userName } = await UserModel.findById(authorId);

        const postDoc = await PostModel.create({
            text, authorId, title, authorName: userName,
        });
        return postDoc;
    }

    static async updatePost(id, { text, authorId, title, likedById }) {
        //for fill authorName in the post
        const { userName } = await UserModel.findById(authorId);

        //for filling likedByName in the post
        const likedByName = [];
        for (let id of likedById) {
            const { userName } = await UserModel.findById(id);
            likedByName.push(userName);
        }

        const postDoc = await PostModel.findByIdAndUpdate(
            id, {
                text, authorId, authorName: userName, title,
            likedById, likedByName
        },{ new: true });
        return postDoc;
    }
}