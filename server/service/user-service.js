const bcrypt = require('bcrypt');
;
const UserModel = require('../models/user-model');
const TokenModel = require('../models/token-model');
const User = require('../temp/user-from-model');
const ApiError = require('../exeptions/api-error');
const tokenService = require('./token-service');

module.exports = class UserService {

    //check if the user already exists;
    //create user in DB,
    //create tokens, save RefreshToken in DB
    //return generated user and tokens
    static async registration(email, password, userName) {
        //check if the user already exists 
        const alreadyUser1 = await UserModel.findOne({ email });
        if (alreadyUser1) {
            throw ApiError.BadRequest(`The user with email ${email} already exists`);
        }
        const alreadyUser2 = await UserModel.findOne({ userName });
        if (alreadyUser2) {
            throw ApiError.BadRequest(`The user with name ${userName} already exists`);
        }

        const hashPassword = bcrypt.hashSync(password, 5);

        const newUserDoc = await UserModel.create({
            email,
            userName,
            password: hashPassword,
        });

        //as soon as the user has logged in or registered
        //generate a pair for it {Access, Refresh}
        //and save Refresh in DB tokenModel
        const user = new User(newUserDoc);
        const tokens = tokenService.generateToken({ ...user });
        await tokenService.saveRefToken(user, tokens.refreshToken);

        return { ...tokens, user: user }
    }

    //if user email not found or wrong password
    //throw error
    //else create tokens, save RefreshToken in DB
    //return the found user and created tokens
    static async login(email, password) {
        //check email
        const userDoc = await UserModel.findOne({ email });
        if (!userDoc) {
            throw ApiError.BadRequest(`User with mail ${email} does not exist`);
        }
        //check password
        const isPassEquals = await bcrypt.compareSync(password, userDoc.password);
        if (!isPassEquals) {
            throw ApiError.BadRequest('Wrong password');
        }

        //as soon as the user has logged in or registered
        //generate a pair for it {Access, Refresh}
        //and save Refresh in DB tokenModel
        const user = new User(userDoc);
        const tokens = tokenService.generateToken({ ...user });
        await tokenService.saveRefToken(user, tokens.refreshToken);

        return { ...tokens, user: user }
    }

    //delete refreshToken from DB
    static async logout(refreshToken) {
        const tokenData = await tokenService.deleteRefToken(refreshToken);
        return tokenData;
    }

    //generate new {Access, Refresh}
    //rewrite refreshToken in DB
    //if the token is invalid or not in the database
    //throw ApiError
    static async refresh(refreshToken) {
        const userValidate = tokenService.validateRefToken(refreshToken);

        const tokenDoc = await TokenModel.findOne({ refreshToken });
        if (!tokenDoc) {
            throw ApiError.Unauthorized('Token not found in DB');
        }

        const userDoc = await UserModel.findById(userValidate.id);
        if (!userDoc) {
            throw ApiError.Unauthorized('User not found in DB, wrong refreshToken');
        }

        const user = new User(userDoc);
        const tokens = tokenService.generateToken({ ...user });
        await tokenService.saveRefToken(user, tokens.refreshToken);

        return { ...tokens, user: user }
    }

    static async getUsers() {
        const users = await UserModel.find();
        return users;
    }
}