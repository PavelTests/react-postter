const jwt = require('jsonwebtoken');
const ApiError = require('../exeptions/api-error');
const tokenModel = require('../models/token-model');
const TokenModel = require('../models/token-model')

class TokenService {
    _accessSecret = process.env.JWT_ACCESS_SECRET || 'secret';
    _refreshSecret = process.env.JWT_REFRESH_SECRET || 'secret';

    //for api/registration or login
    generateToken(payload) {
        console.log('generateTokens: payload', payload);
        const accessToken = jwt.sign(payload, this._accessSecret, { expiresIn: '30m' });
        const refreshToken = jwt.sign(payload, this._refreshSecret, { expiresIn: '1h' });

        return { accessToken, refreshToken }
    }
    //for api/registration or login
    async saveRefToken(user, refreshToken) {
        const tokenData = await TokenModel.findOne({
            userId: user.id,
        })
        if (tokenData) {
            tokenData.refreshToken = refreshToken;
            await tokenData.save();
            return;
        }

        //if the user is logged in for the first time
        await TokenModel.create({
            userId: user.id,
            refreshToken
        })
    }
    //for api/logpout
    async deleteRefToken(refreshToken) {
        const tokenData = await tokenModel.deleteOne({ refreshToken });
        console.log('deleteRefToken', tokenData);
        return tokenData;
    }

    //for api/refresh
    validateRefToken(refreshToken) {
        try {
            const user = jwt.verify(refreshToken, this._refreshSecret);
            return user;
        } catch (error) {
            throw ApiError.Unauthorized('Wrong refreshToken');
        }
    }

    //for api/refresh, auth-middleware
    validateAccessToken(accessToken) {
        try {
            const user = jwt.verify(accessToken, this._accessSecret);
            return user;
        } catch (error) {
            throw ApiError.Unauthorized('Wrong accessToken');
        }
    }
}

module.exports = new TokenService();