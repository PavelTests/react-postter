module.exports = class User {

    constructor({ _id, email, userName }) {
        this.id = _id;
        this.email = email;
        this.userName = userName;
    }
}