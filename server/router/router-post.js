const { Router } = require('express');
//for middleware body validation in request
const { body } = require('express-validator');
const PostControllers = require('../controllers/post-controllers');

//for middleware Authorization header validation
//in request
const authMiddleware = require('../middleware/auth-middleware');

const router = Router();

router.get('/posts', PostControllers.getPosts);

router.post('/add-post',
    authMiddleware,
    body('text').exists(),
    body('authorId').exists(),
    body('title').exists(),
    PostControllers.addPost);

router.put('/update-post/:id',
    authMiddleware,
    body('text').exists(),
    body('authorId').exists(),
    body('title').exists(),
    body('likedById').exists(),
    PostControllers.updatePost)

module.exports = router;