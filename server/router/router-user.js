const { Router } = require('express');
//for middleware body validation in request
const { body } = require('express-validator');
const UserControllers = require('../controllers/user-controllers')

//for middleware Authorization header validation
//in request
const authMiddleware = require('../middleware/auth-middleware');

const router = Router();

//authentication
router.post('/registration',
    body('email').isEmail(),
    body('password').isLength({ min: 4 }),
    body('userName').exists(),
    UserControllers.registration,
)
router.post('/login',
    body('email').exists(),
    body('password').exists(),
    UserControllers.login,
);
router.get('/logout', UserControllers.logout);

router.get('/refresh', UserControllers.refresh);

//data 
router.get('/users',
    authMiddleware,
    UserControllers.getUsers);

module.exports = router;