const ApiError = require('../exeptions/api-error');
const tokenService = require('../service/token-service');
const UserModel = require('../models/user-model');

//middleware to check user authorization 
module.exports = async function (req, res, next) {
    try {
        //Bearer 7t426hjfasfi321i...
        const authHeader = req.headers.authorization;
        if (!authHeader) {
            throw ApiError.Unauthorized('Missing authorization header')
        }
        const accessToken = authHeader.split(' ').pop();
        if (!accessToken) {
            throw ApiError.Unauthorized('Missing accessToken in authorization header')
        }
        const user = tokenService.validateAccessToken(accessToken);
        console.log(user);
        const userDoc = await UserModel.findById(user.id);
        if (!userDoc) {
            throw ApiError.Unauthorized('User not found in DB, wrong accessToken');
        }

        next();
    } catch (error) {
        next(error);
    }

}