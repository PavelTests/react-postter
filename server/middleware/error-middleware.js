const ApiError = require('../exeptions/api-error');

module.exports = function (err, req, res, next) {
    if (err instanceof ApiError) {
        console.log(err.status, err);
        res.status(err.status).json(
            { message: err.message, error: err.errors }
        )
    }
    else {
        console.log('500', err);
        res.status(500).json(
            { message: 'An unexpected error occurred', error: err });
    }
}