import React from 'react'
import { useSelector } from 'react-redux'
import { Navigate } from 'react-router-dom';

function RequireAuth({ children }) {
    console.log('RequireAuth: children', children);
    const { isAuth } = useSelector(state => state.user);

    if (!isAuth) {
        return (
            <Navigate to='/login' />
        )
    }

    return children;
}

export default RequireAuth