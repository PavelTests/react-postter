import React from 'react'
import { useSelector, useDispatch } from 'react-redux';
import style from './LikeButton.module.scss'
import { asyncUpdatePost } from '../../../../store/slice/posts-slice';
import { useNavigate } from 'react-router-dom';

function LikeButton({ post }) {
    const { isAuth, user } = useSelector(state => state.user);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    let likedById = post.likedById;
    //style 
    const isLike = likedById.includes(user.id);
    let className = style.like;
    if (isLike) className += style.active;

    const handleClick = () => {
        if (!isAuth) {
            navigate('/login');
            return;
        }

        if (isLike) {
            likedById = likedById.filter(id => id !== user.id);
        }
        else {
            // console.log(likedById instanceof Array);
            // console.log(user.id);
            likedById = [...likedById, user.id];
        }
        dispatch(asyncUpdatePost({ ...post, likedById }))
    }
    return (
        <button
            className={className}
            onClick={handleClick}><i className="fa fa-heart"></i> Like { likedById.length }
        </button >
    )
}

export default LikeButton