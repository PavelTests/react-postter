import React from 'react'
import LikeButton from './like-button/LikeButton';
import style from './PostItem.module.scss'

function PostItem({ post }) {
    const { title, text, authorName, likedById, createdAt } = post;
    return (
        <li>
            <div className={style.wrapper}>
                <article className={style.article}>
                    <header className={style.header}>
                        <span className={style.title}>{title}</span>
                        <span>{new Date(createdAt).toLocaleDateString()}</span>
                    </header>
                    <p>{text}</p>
                    <div className={style.info}>
                        <span className={style.author}>By: {authorName}</span>
                        <LikeButton post={post} />
                    </div>
                </article>
            </div>
        </li>
    )
}

export default PostItem