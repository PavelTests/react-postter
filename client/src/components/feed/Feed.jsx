import React from 'react'
import { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { asyncFetchPosts } from '../../store/slice/posts-slice';
import Status from '../status/Status';
import style from './Feed.module.scss'
import PostList from './post-list/PostList';
import SortBar from './sort-bar/SortBar';


function Feed() {
    const { posts, error, statusFetching } = useSelector(state => state.posts);
    const dispatch = useDispatch();
    console.log('Feed render');

    const [sort, setSort] = useState('newest');
    useEffect(() => { dispatch(asyncFetchPosts()) }, [dispatch])

    const handleSort = (e) => {
        console.log(e.target);
        setSort(e.target.value);
    }

    return (
        <div>
            <SortBar handleSort={handleSort} sort={sort} />
            <Status error={error} status={statusFetching} />
            {statusFetching === 'fulfilled' &&
                <PostList posts={posts} sort={sort} />
            }
        </div>
    )
}

export default Feed