import React from 'react'
import style from './PostList.module.scss'
import PostItem from '../post-item/PostItem'

function PostList({ posts, sort, user }) {
    let sortedPosts = [];
    switch (sort) {
        case 'newest':
            sortedPosts = posts.slice().sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt));
            break;
        case 'oldest':
            sortedPosts = posts.slice().sort((a, b) => new Date(a.createdAt) - new Date(b.createdAt));
            break;
        case 'mostLiked':
            sortedPosts = posts.slice().sort((a, b) => b.likedById.length - a.likedById.length);
            break;
        case 'leastLiked':
            sortedPosts = posts.slice().sort((a, b) => a.likedById.length - b.likedById.length);
            break;
        case 'byAuthorId':
            sortedPosts = posts.filter(post => post.authorId === user.id);
            break;
        default:
            sortedPosts = [...posts];
            break;
    }

    return (
        <ul className={style.ul}>{sortedPosts.map(post =>
            <PostItem key={post._id} post={post} />)}
        </ul>
    )
}

export default PostList