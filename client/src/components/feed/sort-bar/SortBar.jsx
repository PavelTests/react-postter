import React from 'react'
import style from './SortBar.module.scss'

function SortBar({ handleSort, sort }) {
    return (
        <div className={style.sort_bar}>sort:
            <select value={sort} onChange={e => handleSort(e)}>
                <option value="newest" >newest</option>
                <option value="oldest" >oldest</option>
                <option value="mostLiked" >most liked</option>
                <option value="leastLiked" >least liked</option>
            </select>
        </div>
    )
}

export default SortBar