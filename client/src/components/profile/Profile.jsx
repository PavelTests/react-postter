import React from 'react'
import style from './Profile.module.scss'
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import PostList from '../feed/post-list/PostList';

function Profile() {
    const { user } = useSelector(state => state.user);
    const { posts } = useSelector(state => state.posts);

    const navigate = useNavigate();
    if (!user) {
        navigate('/login');
    }

    return (
        <div className={style.wrapper}>
            <h2>Profile</h2>
            <div className={style.user_info}>
                <i className={'fa fa-user ' + style.profile_pic}></i>
                <div className={style.user_info_text}>
                    <h3>User info: </h3>
                    <div className={style.field}>Username: <div className={style.user_data}>{user.userName}</div></div>
                    <div className={style.field}>Email: <div className={style.user_data}>{user.email}</div></div>
                </div>
            </div>
            <h3>My posts: </h3>
            <PostList posts={posts} user={user} sort='byAuthorId' />
        </div>
    )
}

export default Profile