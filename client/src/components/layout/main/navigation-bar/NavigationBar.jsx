import React from 'react'
import style from './NavigationBar.module.scss'
import { NavLink } from 'react-router-dom'


function NavigationBar() {
  return (
    <nav className={style.nav}>
      <NavLink
        className={({ isActive }) => (isActive ? `${style.active}` : "")} to="">
        Feed
      </NavLink>
      <NavLink className={({ isActive }) => (isActive ? `${style.active}` : "")} to="/add-post">
        Add post
      </NavLink>
      <NavLink className={({ isActive }) => (isActive ? `${style.active}` : "")} to="/profile">
        Profile
      </NavLink>
    </nav>
  )
}

export default NavigationBar