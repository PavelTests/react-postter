import React from 'react'
import style from './Main.module.scss'
import { Routes, Route } from 'react-router-dom';
import NavigationBar from './navigation-bar/NavigationBar';
import SignIn from '../../forms/SignIn';
import SignUp from '../../forms/SingUp';
import Posts from '../../feed/Feed';
import AddPost from '../../forms/AddPost';
import Profile from '../../profile/Profile';
import RequireAuth from '../../RequireAuth';

function Main() {
  return (
    <div className={style.main}>
      <NavigationBar />
      <Routes>
        <Route path='/login' element={<SignIn />} />
        <Route path='/registration' element={<SignUp />} />

        <Route path='/' element={<Posts />} />
        <Route
          path='/add-post'
          element={<RequireAuth><AddPost /></RequireAuth>} />
        <Route
          path='/profile'
          element={<RequireAuth><Profile /></RequireAuth>} />
      </Routes>
    </div>
  )
}

export default Main