import React from 'react'
import style from './Header.module.scss'
import { Link } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { asyncLogout } from '../../../store/slice/user-slice'

function Header() {
    const { isAuth, user } = useSelector(state => state.user);

    const dispatch = useDispatch();
    const handleLogout = () => {
        dispatch(asyncLogout());
    }

    return (
        <div className={style.wrapper}>
            <header className={style.header}>
                <span className={style.logo}><Link to="/">POSTTER</Link></span>
                <nav className={style.nav}>
                    <Link to="/">POSTS</Link>
                    {isAuth ?
                        <>
                            <Link to="/profile">Welcome, {user.userName}</Link>
                            <Link to="/" onClick={handleLogout}>Logout</Link>
                        </>
                        :
                        <>
                            <Link to="/login">Sing in</Link>
                            <Link to="/registration">Sing up</Link>
                        </>
                    }

                </nav>
            </header>
        </div>
    )
}

export default Header