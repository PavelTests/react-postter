import React from 'react'
import style from './Footer.module.scss'

function Footer() {
  return (
    <div className={style.wrapper}>
      <footer className={style.footer}>@PavelInc 2022</footer>
    </div>
  )
}

export default Footer