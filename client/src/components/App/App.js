import style from '../App/App.module.scss';
import Header from '../layout/header/Header';
import Main from '../layout/main/Main';
import Footer from '../layout/footer/Footer';
import { asyncCheckAuth } from '../../store/slice/user-slice'
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';

function App() {
  const dispatch = useDispatch();
  useEffect(() => { dispatch(asyncCheckAuth()) }, [dispatch]);

  return (
    <div className={style.App}>
      <Header />
      <Main />
      <Footer />
    </div>
  );
}

export default App;
