import { useForm } from 'react-hook-form';
import React from 'react'
import style from './form.module.scss'
import { Link, useNavigate } from 'react-router-dom';
import { useEffect } from 'react';
import { asyncLogin } from '../../store/slice/user-slice';
import { useSelector, useDispatch } from 'react-redux'

function SignIn() {
  //form-hook
  const {
    register,
    formState: { errors },
    handleSubmit } = useForm({ mode: 'onBlur' });

  //global state
  const { login: error } = useSelector(state => state.user.errors);
  console.log('SignIn error', error);
  const dispatch = useDispatch();

  const navigate = useNavigate();

  const onSubmit = (data) => {
    const callback = () => navigate('/');
    dispatch(asyncLogin({ ...data, callback }));
  };

  return (
    <div className={style.wrapper}>
      <h2>Sign In</h2>
      <p>Don't have an account? <Link to="/registration">Sign up</Link></p>
      <form onSubmit={handleSubmit(onSubmit)}>
        <label> Email:
          <input
            className={errors.email ? style.input_error : ''}
            type="text"
            {...register('email', {
              required: "*email is required",
              pattern: {
                value: /^\S+@\S+\.\S+$/,
                message: "*email is not valid"
              }
            })} />
        </label>
        <div className={style.text_error}>
          <span>{errors.email?.message}</span>
        </div>

        <label> Password:
          <input
            className={errors.password ? style.input_error : ''}
            type="text"
            {...register('password', {
              required: "*password is required",
              minLength: {
                value: 4,
                message: '*password must include a minimum of four characters'
              }
            })} />
        </label>
        <div className={style.text_error}>
          <span>{errors.password?.message}</span>
        </div>

        <input className='btn' type="submit" value='Login' />
        <p className={style.request_error}>{error}</p>
      </form >
    </div >
  )
}

export default SignIn
