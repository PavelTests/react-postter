import { useForm } from 'react-hook-form';
import React from 'react'
import style from './form.module.scss'
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { asyncAddPost } from '../../store/slice/posts-slice';

function AddPost() {
    const {
        register,
        formState: { errors },
        handleSubmit } = useForm({ mode: 'onBlur' });

    const { isAuth, user } = useSelector(state => state.user);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const onSubmit = (data) => {
        if (!isAuth) {
            navigate('/login');
            return;
        }
        const callback = () => navigate('/');
        dispatch(asyncAddPost({ ...data, authorId: user.id, callback }))
    };
    return (
        <div className={style.wrapper}>
            <h2>Add post</h2>
            <form onSubmit={handleSubmit(onSubmit)}>
                <label> Title:
                    <input
                        className={errors.title ? style.input_error : ''}
                        type="text"
                        {...register('title', {
                            required: "*title is required",
                        })} />
                </label>
                <div className={style.text_error}>
                    <span>{errors.title?.message}</span>
                </div>

                <label> Text:
                    <textarea
                        className={style.textarea + ' ' + (errors.text ? style.input_error : '')}
                        type="text"
                        {...register('text', {
                            required: "*text is required",
                        })} />
                </label>
                <div className={style.text_error}>
                    <span>{errors.text?.message}</span>
                </div>

                <input className='btn' type="submit" value='Add post' />
            </form >
        </div >
    )
}

export default AddPost