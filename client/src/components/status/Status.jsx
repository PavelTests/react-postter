import React from 'react'

function Status({ status, error }) {
    return (
        <div>
            {error && <>Error: {error}</>}
            {status === 'pending' && "Loading..."}
        </div>
    )
}

export default Status