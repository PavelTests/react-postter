import { configureStore } from '@reduxjs/toolkit'
import userReducer from './slice/user-slice';
import postsReducer from './slice/posts-slice';

export default configureStore({
    reducer: {
        user: userReducer,
        posts: postsReducer,
    },
})
