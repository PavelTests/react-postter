import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import apiAuth from '../../http/apiAuth';

export const asyncFetchPosts = createAsyncThunk(
    'posts/asyncFetchPosts',
    async function (_, { rejectWithValue }) {
        try {
            const res = await apiAuth.get('/posts');
            return res.data;
        } catch (error) {
            return rejectWithValue(error.message);
        }
    })

export const asyncUpdatePost = createAsyncThunk(
    'posts/asyncUpdatePost',
    async function ({ _id, text, authorId, title, likedById },
        { rejectWithValue, dispatch }) {
        try {
            const res = await apiAuth.put(`/update-post/${_id}`, { text, authorId, title, likedById })
            const post = res.data;
            dispatch(updatePost(post));

            return post;
        } catch (error) {
            return rejectWithValue(error.message);
        }
    })

export const asyncAddPost = createAsyncThunk(
    'posts/asyncAddPost',
    async function ({ text, authorId, title, callback },
        { rejectWithValue, dispatch }) {
        try {
            const res = await apiAuth.post('/add-post', { text, authorId, title })
            const post = res.data;

            dispatch(addPost(post))
            callback()
            return post;
        } catch (error) {
            return rejectWithValue(error.message);
        }
    })


const initialState = {
    posts: [],
    errors: {
        addPost: null,
        fetchPosts: null,
        updatePost: null,
    },
    statusFetching: 'idle',
}

const userSlice = createSlice({
    name: 'posts',
    initialState,
    reducers: {
        addPost(state, action) {
            console.log('addPost', action.payload);
            if (action.payload) {
                state.posts.push(action.payload);
            }

            state.status = 'fulfilled';
            state.errors = {};
        },
        updatePost(state, action) {
            console.log('updatePost', action.payload);
            const newPost = action.payload;
            if (newPost) {
                state.posts = state.posts.map(post =>
                    post._id === newPost._id ? newPost : post
                );
            }

            state.status = 'fulfilled';
            state.errors = {};
        }
    },
    extraReducers: {
        [asyncFetchPosts.pending]: (state) => {
            state.statusFetching = 'pending';
        },
        [asyncFetchPosts.fulfilled]: (state, action) => {
            state.errors.fetchPosts = null;
            state.statusFetching = 'fulfilled';
            state.posts = action.payload;
            console.log('asyncFetchPosts.fulfilled', action.payload);
        },

        [asyncFetchPosts.rejected]: (state, action) => {
            state.statusFetching = 'rejected';
            state.errors.fetchPosts = action.payload;
            console.log('asyncFetchPosts.rejected', action.payload);
        },
        [asyncUpdatePost.rejected]: (state, action) => {
            state.errors.updatePost = action.payload;
            console.log('asyncUpdatePost.rejected', action.payload);
        },
        [asyncAddPost.rejected]: (state, action) => {
            state.errors.addPost = action.payload;
            console.log('asyncAddPost.rejected', action.payload);
        },
    }
});

export const { addPost, updatePost } = userSlice.actions
export default userSlice.reducer;