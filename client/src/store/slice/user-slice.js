import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import api from '../../http/api';

export const asyncCheckAuth = createAsyncThunk(
    'user/asyncCheckAuth',
    async function (_,
        { rejectWithValue, dispatch }) {
        try {
            const res = await api.get('/refresh');
            localStorage.setItem('accessToken', res.data.accessToken);
            const user = res.data.user;

            dispatch(setUser(user));
            return user;
        } catch (error) {
            return rejectWithValue(error?.response?.data?.message || error.message);
        }
    });

export const asyncRegistration = createAsyncThunk(
    'user/asyncRegistration',
    async function ({ email, password, userName, callback },
        { rejectWithValue, dispatch }) {
        try {
            const res = await api.post('/registration', { email, password, userName });
            localStorage.setItem('accessToken', res.data.accessToken);
            const user = res.data.user;

            dispatch(setUser(user));
            callback();
            return user;
        } catch (error) {
            return rejectWithValue(error?.response?.data?.message || error.message);
        }
    })

export const asyncLogin = createAsyncThunk(
    'user/asyncLogin',
    async function ({ email, password, callback },
        { rejectWithValue, dispatch }) {
        try {
            const res = await api.post('/login', { email, password });
            localStorage.setItem('accessToken', res.data.accessToken);
            const user = res.data.user;

            dispatch(setUser(user));
            callback();
            return user;
        } catch (error) {
            return rejectWithValue(error?.response?.data?.message || error.message);
        }
    })

export const asyncLogout = createAsyncThunk(
    'user/asyncLogout',
    async function (_, { rejectWithValue, dispatch }) {
        try {
            const res = await api.get('/logout');
            localStorage.removeItem('accessToken');
            const token = res.data;

            return token;
        } catch (error) {
            return rejectWithValue(error?.response?.data?.message || error.message);
        }
        finally {
            dispatch(removeUser());
        }
    })

const initialState = {
    user: {},
    errors: {
        checkAuth: null,
        login: null,
        registration: null,
        logout: null,
    },
    isAuth: false,
    status: 'idle',
}

const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        setUser(state, action) {
            state.errors = {};
            state.isAuth = true;
            state.user = action.payload;
            console.log(action.type, action.payload);
        },
        removeUser(state) {
            state.user = {};
            state.isAuth = false;
            state.status = 'idle';
        }
    },
    extraReducers: {
        [asyncCheckAuth.pending]: (state) => {
            state.status = 'pending';
        },
        [asyncCheckAuth.fulfilled]: (state, action) => {
            state.errors.checkAuth = null;
            state.status = 'fulfilled';
            console.log(action.payload);
        },
        [asyncCheckAuth.rejected]: (state, action) => {
            state.status = 'rejected';
            state.errors.checkAuth = action.payload;
            console.log('asyncCheckAuth.rejected', action.payload);
        },
        [asyncRegistration.rejected]: (state, action) => {
            state.status = 'rejected';
            state.errors.registration = action.payload;
            console.log('asyncRegistration.rejected', action.payload);
        },
        [asyncLogin.rejected]: (state, action) => {
            state.status = 'rejected';
            state.errors.login = action.payload;
            console.log('asyncLogin.rejected', action.payload);
        },
        [asyncLogout.rejected]: (state, action) => {
            state.status = 'rejected';
            state.errors.logout = action.payload;
            console.log('asyncLogout.rejected', action.payload);
        },
    }
});

export const { setUser, removeUser } = userSlice.actions
export default userSlice.reducer;
