import axios from 'axios';

const SERVER_URL = process.env.SERVER_URL || 'http://localhost:5000/api';
const apiAuth = axios.create({
    //automatically add cookies to request 
    withCredentials: true,
    baseURL: SERVER_URL,
})

//interceptors for request to server
apiAuth.interceptors.request.use((config) => {
    const accessToken = localStorage.getItem('accessToken');
    if (accessToken) {
        config.headers = config.headers ?? {};
        config.headers.Authorization = `Bearer ${accessToken}`;
    }

    return config;
}, (error) => {
    throw error;
});

export default apiAuth;