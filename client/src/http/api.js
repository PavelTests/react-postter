import axios from 'axios';

const SERVER_URL = process.env.SERVER_URL || 'http://localhost:5000/api';
const api = axios.create({
    //automatically add cookies to request 
    withCredentials: true,
    baseURL: SERVER_URL,
})

export default api;