# react-postter

Блог - SPA c использованием mongoDB, JWT-авторизацией и API на сервере

* Стек React
* Функциональные компоненты, React-hooks
* Модульные стили или styled-components
* Роутинг React-router-dom
* ReduxToolKit. Асинхронные экшены Redux-thunk
* Работа с API с помощью клиента Axios

### Быстрый старт

```
cd client
npm i
npm run start
```
```
cd server
npm i
npm run start
```
### Возможности
* Создание/валидация пользователей
* Создание постов и оценок
* Собственный профиль
